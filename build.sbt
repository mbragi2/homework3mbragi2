name := "StockQuotes2018"

version := "1.0"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "net.liftweb" %% "lift-json" % "3.1.1"
)

scalacOptions += "-deprecation"
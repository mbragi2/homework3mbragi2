mbragi2 HW3 CS441

In this HW we were asked to analyze historical stock data of stocks. After some research and reading online,
I chose to use Alpha Vantage API as many developers were saying that it is currently the best historical finance
API. It allows you to simply return json for easy parsing as well as even give you the option to send the data
to a CSV file for analysis there.

How to Run:
Simply create a new Intellij project that runs scala programs. Grab my files: Finance.scala and build.sbt.
After you have that set up you can run Finance.scala and you will see the data come from the API. I spent a lot
of time trying to push my code to Spark and running it on there but I had a lot of issues with VMware so I did
not deploy my program to there and only managed to run it locally. I used to lift-JSON to try and parse the json
that was returned by the API.